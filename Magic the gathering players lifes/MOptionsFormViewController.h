//
//  MOptionsFormViewController.h
//  Magic the gathering players lifes
//
//  Created by Igogo on 4/1/14.
//  Copyright (c) 2014 Igogo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MViewController;

@interface MOptionsFormViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>
{
    MViewController *parentCtrl;
}

@property (nonatomic, retain) MViewController *parentCtrl;

@property (weak, nonatomic) IBOutlet UIImageView *changeBG1Item;
@property (weak, nonatomic) IBOutlet UIImageView *changeBG2Item;
@property (weak, nonatomic) IBOutlet UILabel *dice;

@property (strong, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UIButton *diceRoller;

- (IBAction)closeAndSave:(id)sender;

- (IBAction)changeBG1:(id)sender;
- (IBAction)changeBG2:(id)sender;

- (IBAction)rollDice:(id)sender;

-(void)initImages;
-(void)initHps;

@end
