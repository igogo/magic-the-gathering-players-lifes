//
//  MAppDelegate.h
//  Magic the gathering players lifes
//
//  Created by Igogo on 3/30/14.
//  Copyright (c) 2014 Igogo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

-(void)initializeStoryBoardBasedOnScreenSize;

@end
