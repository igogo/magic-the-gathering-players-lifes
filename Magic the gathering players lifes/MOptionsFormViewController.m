//
//  MOptionsFormViewController.m
//  Magic the gathering players lifes
//
//  Created by Igogo on 4/1/14.
//  Copyright (c) 2014 Igogo. All rights reserved.
//

#import "MOptionsFormViewController.h"
#import "MViewController.h"

@interface MOptionsFormViewController (){
    NSArray * images;
    NSArray * hps;
    NSString * img1;
    NSString * img2;
    id selectedHp;
    int i1;
    int i2;
}

@end

@implementation MOptionsFormViewController
@synthesize parentCtrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initImages];
    [self initHps];
    
    self->i1 = 0;
    self->i2 = 1;
    
    self.changeBG1Item.image = [UIImage imageNamed: self->images[self->i1]];
    self.changeBG2Item.image = [UIImage imageNamed: self->images[self->i2]];
    
    self->img1 = self->images[ self->i1 ];
    self->img2 = self->images[ self->i2 ];
    self->selectedHp = [self->hps objectAtIndex:0];
    
    self.picker.delegate = self;
    self.picker.dataSource = self;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    return [self->hps count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self->hps objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component
{
    self->selectedHp = [self->hps objectAtIndex:row];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)closeAndSave:(id)sender {
    [[parentCtrl options_dict ]setObject:self->img1 forKey:@"bg1"];
    [[parentCtrl options_dict ]setObject:self->img2 forKey:@"bg2"];
    [[parentCtrl options_dict ]setObject:self->selectedHp forKey:@"hp"];
    
    
    [parentCtrl initPlayers];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)changeBG1:(id)sender {
    self->i1++;
    if(self->i1 >= [ self->images count] ){
        self->i1 = 0;
    }
    
    self->img1 = self->images[ self->i1 ];
    self.changeBG1Item.image = [UIImage imageNamed: self->images[ self->i1 ]];
}

- (IBAction)changeBG2:(id)sender {
    self->i2++;
    if(self->i2 >= [ self->images count] ){
        self->i2 = 0;
    }
    
    self->img2 = self->images[ self->i2 ];
    self.changeBG2Item.image = [UIImage imageNamed: self->images[ self->i2 ]];
}

- (IBAction)rollDice:(id)sender {
    int roll = (arc4random() % 20) + 1;
    self.dice.hidden = NO;
    self.dice.text = [NSString stringWithFormat:@"%i", roll];
}

-(void)initImages{
    self->images = [NSArray arrayWithObjects:
                    @"bg1.jpg",
                    @"bg2.jpg",
                    nil];
}

-(void)initHps{
    self->hps = [NSArray arrayWithObjects:
                 @"20",
                 @"30",
                 @"40",
                 @"50",
                 nil];
}

@end
