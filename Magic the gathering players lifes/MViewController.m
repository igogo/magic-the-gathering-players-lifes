//
//  MViewController.m
//  Magic the gathering players lifes
//
//  Created by Igogo on 3/30/14.
//  Copyright (c) 2014 Igogo. All rights reserved.
//

#include <math.h>
#import "MViewController.h"

@interface MViewController ()

@end

@implementation MViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //DESIBLE SLEEP MODE
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    self.isConfigured = NO;
    
    self.options_dict = [[NSMutableDictionary alloc] init];
    self.options_dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"bg1.jpg", @"bg1",
                                 @"bg2.jpg", @"bg2",
                                 [NSNumber numberWithInt:20], @"hp",
                                 nil];
    
    [self initPlayers];
    
    [self life_2].transform = CGAffineTransformMakeRotation (M_PI);
}

- (void) initPlayers{
    NSLog(@"%@", self.options_dict);
    
    self.i1 = [[self.options_dict objectForKey:@"hp"] intValue];
    self.i2 = [[self.options_dict objectForKey:@"hp"] intValue];
    [[self life_1] setText:[NSString stringWithFormat:@"%i", self.i1]];
    [[self life_2] setText:[NSString stringWithFormat:@"%i", self.i2]];
    
    self.bg_1.image = [UIImage imageNamed: self.options_dict[@"bg1"]];
    
    UIImage* sourceImage = [UIImage imageNamed: self.options_dict[@"bg2"]];
    self.bg_2.image = [UIImage imageWithCGImage:sourceImage.CGImage
                                          scale:sourceImage.scale orientation: UIImageOrientationDownMirrored];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(!self.isConfigured){
//        [self openOptions];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)plus_1:(id)sender {
    self.i1++;
    [[self life_1] setText:[NSString stringWithFormat:@"%i", self.i1]];
}

- (IBAction)plus_2:(id)sender {
    self.i2++;
    [[self life_2] setText:[NSString stringWithFormat:@"%i", self.i2]];
}

- (IBAction)minus_1:(id)sender {
    self.i1--;
    [[self life_1] setText:[NSString stringWithFormat:@"%i", self.i1]];
}

- (IBAction)minus_2:(id)sender {
    self.i2--;
    [[self life_2] setText:[NSString stringWithFormat:@"%i", self.i2]];
}

- (IBAction)options:(id)sender {
    [self openOptions];
}

- (void)openOptions{
    MOptionsFormViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"options_form"];
	viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    viewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    viewController.parentCtrl = self;
    [self presentModalViewController:viewController animated:YES];
}


@end
