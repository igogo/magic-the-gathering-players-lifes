//
//  MViewController.h
//  Magic the gathering players lifes
//
//  Created by Igogo on 3/30/14.
//  Copyright (c) 2014 Igogo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOptionsFormViewController.h"

@interface MViewController : UIViewController

@property int i1;
@property int i2;
@property bool isConfigured;

@property NSMutableDictionary * options_dict;

@property (weak, nonatomic) IBOutlet UILabel *life_1;
@property (weak, nonatomic) IBOutlet UILabel *life_2;

@property (weak, nonatomic) IBOutlet UIImageView *bg_1;
@property (weak, nonatomic) IBOutlet UIImageView *bg_2;

- (IBAction)plus_1:(id)sender;
- (IBAction)plus_2:(id)sender;
- (IBAction)minus_1:(id)sender;
- (IBAction)minus_2:(id)sender;
- (IBAction)options:(id)sender;

- (void)openOptions;
- (void)initPlayers;

@end
