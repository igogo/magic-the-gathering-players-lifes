//
//  main.m
//  Magic the gathering players lifes
//
//  Created by Igogo on 3/30/14.
//  Copyright (c) 2014 Igogo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MAppDelegate class]));
    }
}
